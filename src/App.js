import { Developer } from "./components/developer";


function App() {
  return (
    <div className="App">
      <Developer 
      name="Lucas"
      age="12"
      country="Brasil"
      />
      <Developer 
      name="Pedro"
      age="21"
      country="Brasil"
      />
    </div>
  );
}

export default App;
